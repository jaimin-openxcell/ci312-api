<?php 
include APPPATH . 'Transformers/Transformer.php';
class UsersTransformer extends Transformer
{
	public function transform($user){	
		return [
			"id"   => $user['id'],
			"name" => $user['name']
		];
	}
}