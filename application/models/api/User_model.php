<?php defined('BASEPATH') OR exit('No direct script access allowed');
include 'Api.php';
class User_model extends Api {
    public function __construct() {
        parent::__construct();
        $this->table = 'users';
    }
    
    /**
     * [all : Simple fetch data - No pagination]
     * @return [Arr] [Array of result]
     */
    public function all(){
        $query=$this->db->query("SELECT * FROM $this->table");
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return array();
    }
    /**
     * [allPaginate : With pagination]
     * @return [ARR] [Array of user with pagination]
     * paginate is a method declared in parant class.
     */
    public function allPaginate(){
        # BASE QUERY TO PAGINATE : used in data and pagination fetching
        $where      ="$this->table.status != 'inactive'";

        # PAGINATION SET : fetch pagination
        $pagination =$this->paginate($this->table,$where);
        $offset     =($this->page * $this->limit) - $this->limit;

        # QUERY PAGINATION : for fetching data
        $restrict="$offset,$this->limit";

        # QUERY FOR RESULT DATA
        $query=$this->db->query("SELECT 
            *
            FROM 
            $this->table 
            WHERE $where 
            LIMIT $restrict");
        // echo $this->db->last_query();exit;
        
        #If row found then fetch results
        if($query->num_rows()>0){
            # Setting up result with pagination
            $result=array(
                "data"       =>$query->result_array(),
                "pagination" =>$pagination
            );
            return $result;
        }
        return array();
    }
}

/* End of file User_model.php */
/* Location: ./application/models/api/User_model.php */