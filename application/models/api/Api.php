<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Model {
	public function __construct() {
        parent::__construct();
        $this->page=1;
        $this->limit=API_PAGINATE_PERPAGE;
    }
	 /**
     * [limit : limit setter]
     * @param  [int] $limit [offset]
     * @return [obj]        [obj]
     */
    public function limit($limit){
    	$this->limit=$limit;
    	return $this;
    }
    /**
     * [page : Page setter]
     * @param  [int] $page [page]
     * @return [obj]       [obj]
     */
    public function page($page){
    	$this->page=$page;
    	return $this;
    }
    /**
     * [paginate : Pagination setter of the table]
     * @param  [str] $table [name of table]
     * @param  [str] $where [where condition]
     * @param  array  $join  [array for join query- key is table name and value will be match b/w table keys]
     * @return [ARR]        [Arr of pagination]
     */
    public function paginate($table,$where,$join=array()){
    	$prepare=$this->db;
        # Check condition
    	if($where!=""){
    		$prepare->where($where);
    	}
        $prepare->from($table);
        # Refering joins in query
        if(count($join) > 0){
            foreach ($join as $table=>$condition) {
                $prepare->join($table, $condition);
            }
        }
        
    	$totalResults=$prepare->count_all_results();
    	$calcPage=ceil($totalResults/$this->limit);
    	$offset=($this->page * $this->limit) - $this->limit;
    	$paginate=array(
			"total_results"        => $totalResults,
			"pages"        => $calcPage,
			"current_page" => $this->page,
            "last_page"    => $calcPage,
			"per_page"     => (Integer)$this->limit,
			//"offset"       => $offset
    	);
    	// mprd($paginate);
    	return $paginate;
    }
}

/* End of file Api.php */
/* Location: ./application/models/api/Api.php */