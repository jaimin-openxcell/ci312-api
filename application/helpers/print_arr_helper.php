<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mpr'))
{
    function mpr($d, $echo = TRUE)
	{
	   if($echo)
	   {
		   echo '<pre>'.print_r($d, true).'</pre>';
	   }
	   else
	   {
	      return '<pre>'.print_r($d, true).'</pre>';
	   } 
	}
}

if ( ! function_exists('mprd'))
{
    function mprd($d) 
	{ 	
	   mpr($d); 	
	   die; 
	}
}

if ( ! function_exists('mvr'))
{
    function mvr($d) 	
	{
	   echo '<pre>'.var_dump($d, true).'</pre>'; 
	}
}

if ( ! function_exists('mvrd'))
{
    function mvrd($d) 
	{
	   mvr($d);
	   die; 
	}
}
if(! function_exists('array_exists')){
	function array_exists($array, $key){
		if ($array instanceof ArrayAccess) {
	        return $array->offsetExists($key);
	    }

	    return array_key_exists($key, $array);
	}
}

if(! function_exists('array_except')){
	function array_except(&$array, $keys)
    {
        $original = &$array;
        
        $keys = (array) $keys;

        if (count($keys) === 0) {
            return;
        }

        foreach ($keys as $key) {
            // if the exact key exists in the top-level, remove it
            if (array_exists($array, $key)) {

                unset($array[$key]);

                continue;
            }

            $parts = explode('.', $key);

            // clean up before each pass
            $array = &$original;

            while (count($parts) > 1) {
                $part = array_shift($parts);

                if (isset($array[$part]) && is_array($array[$part])) {
                    $array = &$array[$part];
                } else {
                    continue 2;
                }
            }

            unset($array[array_shift($parts)]);
        }
        return $array;
    }
}
if(! function_exists('array_only')){
    function array_only($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
}
if ( ! function_exists( 'array_pluck' ) ) {
    function array_pluck($arr,$toPlucked) {
        return array_map(function($arr) use($toPlucked){
                return $arr["$toPlucked"];
            },$arr);
    }
}
if(! function_exists('validate_parameter')){
    function validate_parameter($request,$required){
        return count(array_diff($required, array_keys($request)))==0?1:0;
    }
}
?>