<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/Exceptions/ExceptionHandler.php';
class Apicontroller extends REST_Controller {
	
	protected $statuscode=200;
	protected $ci;
	public function __construct()
	{
		parent::__construct();
		$this->setAuthkey($this->head('authkey'));
		$this->ci    = & get_instance();
		$this->table = $this->ci->config->config['auth_table'];
		$this->exception=new ExceptionHandler;
		$this->checktable();
	}
	private function checktable(){
		try{
			if(! $this->ci->db->table_exists($this->table) ){
				throw new ExceptionHandler($this->table. " does not exits! ");
			}
			return true;
		}
		catch(ExceptionHandler $e){
			echo $e->getMessage();exit;
		}
	}
	/**
	 * [getAuthkey : AuthKey Getter Method]
	 * @return [type] [description]
	 */
	public function getAuthkey(){
		return $this->authkey;
	}
	/**
	 * [setAuthkey : AuthKey Setter Method]
	 * @param [type] $authkey [description]
	 */
	protected function setAuthkey($authkey){
		$this->authkey=$authkey;
		return $this;	
	}
	protected function auth(){
		$authKey=$this->getAuthkey();
		if($this->table!="" && $authKey!=""){
			$this->ci->db->select('*');
			$this->ci->db->from("$this->table");
			$this->ci->db->where('authKey=',$authKey);
			$query = $this->ci->db->get();
			if($query->num_rows()==1){
				return $query->row();
			}
		}
		else{
			return array();
		}
	}
	protected function checkActive($user,$statusFiled='status'){
		if(count($user) > 0){
			if(isset($user->{$statusFiled}) && strtolower($user->{$statusFiled})=="active"){
				return '';
			}
			else{
				return $this->respondStatusError();
			}
		}
		return $this->respondNotFound();
	}
	

	public function getStatuscode(){
		return $this->statuscode;
	}
	public function setStatuscode($statuscode){
		$this->statuscode=$statuscode;
		return $this;	
	}
	public function respondNotFound($message = ""){
		$message=($message=='')?$this->lang->line('NOT_FOUND'):$message;
		return $this->setStatuscode(404)->respondWithError($message);
	}	
	public function respondInternalError($message = ""){
		$message=($message=='')?$this->lang->line('INTERNAL_ERROR'):$message;
		return $this->setStatuscode(500)->respondWithError($message);
	}
	public function respondAuthError($message = ""){
		$message=($message=='')?$this->lang->line('UNAUTH_ACCESS'):$message;
		return $this->setStatuscode(401)->respondWithError($message);
	}
	public function respondStatusError($message = ""){
		$message=($message=='')?$this->lang->line('ACCOUNT_INACTIVE'):$message;
		return $this->setStatuscode(403)->respondWithError($message);
	}
	public function respondValidationError($message = "",$details=array()){		
		$message=($message=='')?$this->lang->line('VALIDATION_FAIL'):$message;
		return $this->setStatuscode(422)->respondWithError($message,$details);
	}
	public function respond($data,$message="Success",$headers=array()){
		$data['status_code']=$this->getStatuscode();
		$data['message']=$message;
		if(count($headers) > 0){
			foreach ($headers as $key => $value) {
				header("$key: $value");
			}
		}
		$this->set_response($data, $this->setStatus());
	}

	public function respondWithError($message,$details=array()){
		$data['status_code'] =422;
		$data['error']=[
			'message'=>$message,
			'details'=>$details
		];
	    $this->set_response($data, $this->setStatus());
	}
	public function respondCreated($message){
		return $this->setStatuscode(201)->respond(['message'=>$message]);
	}
	protected function setStatus(){
		$status=$this->getStatuscode();
		switch ($status) {
			case 200:
				return REST_Controller::HTTP_OK;
				break;
			case 404:
				return REST_Controller::HTTP_NOT_FOUND;
				break;
			case 400:
				return REST_Controller::HTTP_BAD_REQUEST;
				break;
			case 201:
				return REST_Controller::HTTP_CREATED;
				break;
			case 204:
				REST_Controller::HTTP_NO_CONTENT;
				break;
			default:
				return REST_Controller::HTTP_OK;
				break;
		}
	}

}

/* End of file Apicontroller.php */
/* Location: ./application/controllers/api/Apicontroller.php */