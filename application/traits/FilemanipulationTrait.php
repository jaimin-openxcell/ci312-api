<?php  

if (!trait_exists('FilemanipulationTrait')) {
    trait FilemanipulationTrait
    {
    	/**
    	 * [localUpload description]
    	 * @param  [type]  $fileObj    [description]
    	 * @param  [type]  $inputname  [description]
    	 * @param  [type]  $directory  [description]
    	 * @param  string  $allowFiles [description]
    	 * @param  integer $max_size   [description]
    	 * @return [type]              [description]
    	 */
    	public function localUpload($fileObj, $inputname, $directory, $allowFiles='*',$max_size=0){
			$storeName=$this->generateFileName($fileObj,$inputname);

			$targetpath = UPLOADS . "$directory/";
	        if (!is_dir($targetpath)){
	            mkdir($targetpath, 0777, TRUE);
	            // $this->copyEssential($directory);

	        }
	        // Upload Configuration
			$config['upload_path']   = $targetpath;
			$config['file_name']     = $storeName;
			$config['allowed_types'] = $allowFiles;
			$config['max_size']      = $max_size;
			$config['overwrite']     = false;
			// Pre-loading Lib
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	       
	        if ($this->upload->do_upload("$inputname")) {
	            extract($this->upload->data());
	            return $storeName;
	        } else {
	            $err = array('0' => $this->upload->display_errors());
	            return $err;
	        }
		}

    	public function generateFileName($file,$inputname){
			$fileOrgName =clean($file[$inputname]['name']);
			$fileExt     =pathinfo($file[$inputname]['name'], PATHINFO_EXTENSION);
			$fileName=$fileOrgName."_".time().'.'.$fileExt;

			if(strlen($fileName)>=255){
				$fileName=time().'.'.$fileExt;
			}
			return $fileName;
		}

		public function delFile($dir,$filename){
			if($filename!=""){
				$fullPath=UPLOADS."$dir/".$filename;
				if(file_exists($fullPath)){
					unlink($fullPath);
				}
			}
		}

		public function removeDir($dir){
			$fullPath=UPLOADS."$dir";
			if (is_dir($fullPath)) {
				exec("rm -rf {$fullPath}");
			}
		}
		
		protected function copyEssential($dir){
			$file=DOC_ROOT.'default/index.php';
			$newfile=UPLOADS."$dir/".'index.php';

	        copy($file, $newfile);
		}
    }
}